<?php

if (false === class_exists('EnvironmentVariableNotExistsException')) {
    /**
     * Class EnvironmentVariableNotExistsException
     */
    class EnvironmentVariableNotExistsException extends Exception
    {
        /**
         * Construct the exception. Note: The message is NOT binary safe.
         *
         * @link  https://php.net/manual/en/exception.construct.php
         *
         * @param string $name
         *
         * @since 5.1.0
         */
        public function __construct(string $name)
        {
            parent::__construct(
                'Environment Variable "' . $name . '" not found!'
            );
        }
    }
}

if (false === function_exists('env')) {
    /**
     * @param string $name
     *
     * @return array|false|string
     * @throws EnvironmentVariableNotExistsException
     */
    function env(string $name) {
        $result = getenv($name);

        if (false === $result) {
            throw new EnvironmentVariableNotExistsException($name);
        }

        return $result;
    }
}
