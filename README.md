## Setup .env from reactapp/.env

````
REACT_APP_JSON_DATA=https://mgtechtest.blob.core.windows.net/files/showcase.json
```` 

---

## Available Scripts

In the project directory, you can run:

### To build/start docker containers

````
docker-compose up
````

This step will automatically install node_modules and start the project.

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

API is available at [http://localhost:8080/api/doc](http://localhost:8080/api/doc)

---

### Initial setup

````
sh setup.sh
````

or

````
docker exec -it imapi_php composer install
docker exec -it imapi_php php bin/console doctrine:mongodb:schema:update --dm=default
docker exec -it imapi_php chmod 0777 var -R
````

---

### To run the queue system


````
docker exec -it imapi_php bin/console messenger:consume ingest download -vv
````

or

````
sh queue.sh
````

---

### To run the unit tests

````
docker exec -it imapi_php bin/phpunit
````

or

````
sh test.sh
````
