#!/bin/bash

docker exec -it imapi_php composer install
docker exec -it imapi_php php bin/console doctrine:mongodb:schema:update --dm=default
docker exec -it imapi_php chmod 0777 var -R
