<?php

namespace App\Exception;

use Exception;

/**
 * Class CannotCreateMovieException
 *
 * @package App\Exception
 */
class CannotCreateMovieException extends Exception
{
    /**
     * JsonParseError constructor.
     *
     * @param string $error
     */
    public function __construct(string $error)
    {
        parent::__construct($error, 400);
    }
}
