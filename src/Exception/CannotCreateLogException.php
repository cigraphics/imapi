<?php

namespace App\Exception;

use Exception;

/**
 * Class CannotCreateLogException
 *
 * @package App\Exception
 */
class CannotCreateLogException extends Exception
{
    /**
     * JsonParseError constructor.
     *
     * @param string $error
     */
    public function __construct(string $error)
    {
        parent::__construct($error, 400);
    }
}
