<?php

namespace App\Exception;

use Exception;

/**
 * Class CannotUpdateMovieException
 *
 * @package App\Exception
 */
class CannotUpdateMovieException extends Exception
{
    /**
     * JsonParseError constructor.
     *
     * @param string $error
     */
    public function __construct(string $error)
    {
        parent::__construct($error, 400);
    }
}
