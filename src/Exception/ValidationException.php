<?php

namespace App\Exception;

use InvalidArgumentException;

/**
 * Class JsonParseError
 *
 * @package App\Exception
 */
class ValidationException extends InvalidArgumentException
{
    /**
     * JsonParseError constructor.
     *
     * @param string $error
     */
    public function __construct(string $error)
    {
        parent::__construct($error, 400);
    }
}
