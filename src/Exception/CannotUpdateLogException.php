<?php

namespace App\Exception;

use Exception;

/**
 * Class CannotUpdateLogException
 *
 * @package App\Exception
 */
class CannotUpdateLogException extends Exception
{
    /**
     * JsonParseError constructor.
     *
     * @param string $error
     */
    public function __construct(string $error)
    {
        parent::__construct($error, 400);
    }
}
