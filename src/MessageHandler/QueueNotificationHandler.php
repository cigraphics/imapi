<?php

namespace App\MessageHandler;

use App\Document\Movie;
use App\Exception\CannotCreateMovieException;
use App\Message\DownloadNotification;
use App\Message\QueueNotification;
use App\Repository\LogRepository;
use App\Repository\MovieRepository;
use App\Util\Download;
use Exception;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use SplFileInfo;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * Class QueueNotificationHandler
 *
 * @package App\MessageHandler
 */
class QueueNotificationHandler implements MessageHandlerInterface
{
    private MovieRepository $movieRepository;
    private Download $download;
    private MessageBusInterface $bus;
    private LogRepository $logRepository;

    /**
     * QueueNotificationHandler constructor.
     *
     * @param MovieRepository     $movieRepository
     * @param Download            $download
     * @param MessageBusInterface $bus
     * @param LogRepository       $logRepository
     */
    public function __construct(
        MovieRepository $movieRepository,
        Download $download,
        MessageBusInterface $bus,
        LogRepository $logRepository
    ) {
        $this->movieRepository = $movieRepository;
        $this->download = $download;
        $this->bus = $bus;
        $this->logRepository = $logRepository;
    }

    /**
     * @param QueueNotification $message
     */
    public function __invoke(QueueNotification $message)
    {
        $data = json_decode($message->getContent(), true);
        if (true === $data['clear']) {
            try {
                $this->movieRepository->removeAll();
                $images = new RecursiveIteratorIterator(
                    new RecursiveDirectoryIterator(env('STORAGE_PATH'), RecursiveDirectoryIterator::SKIP_DOTS),
                    RecursiveIteratorIterator::CHILD_FIRST
                );
                /**
                 * @var SplFileInfo $image
                 */
                foreach ($images as $image) {
                    if ($image->isFile()) {
                        unlink($image->getPath() . '/' . $image->getFilename());
                    }
                }
            } catch (Exception $e) {
                echo $e->getMessage();
            }
        }

        try {
            $items = $this->download->getJson($data['url']);
            $this->processItems($items, $data['logId']);
        } catch (Exception $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    /**
     * @param array  $items
     * @param string $logId
     *
     * @throws Exception
     */
    private function processItems(array $items, string $logId)
    {
        $i = 0;
        foreach ($items as $item) {
            $movie = new Movie();
            $movie->setIdentifier($item['id']);
            $movie->setHeadline($item['headline']);
            $date = explode('-', $item['lastUpdated']);
            $movie->setLastUpdated((new \DateTime())->setDate($date[0], $date[1], $date[2]));

            $images = [
                'identifier' => $item['id'],
                'logId'      => $logId,
                'itemNo'     => $i,
                'images'     => [
                    'cardImages'   => $item['cardImages'] ?? null,
                    'keyArtImages' => $item['keyArtImages'] ?? null
                ]
            ];

            unset($item['id'], $item['headline'], $item['lastUpdated'], $item['cardImages'], $item['keyArtImages']);
            $movie->setMetadata($item);

            try {
                $result = $this->movieRepository->create($movie);
                $images['id'] = $result->getId();
                $this->bus->dispatch(new DownloadNotification(json_encode($images)));
                ++$i;
            } catch (CannotCreateMovieException $e) {
                echo $e->getMessage();
                continue;
            } catch (Exception $e) {
                echo $e->getMessage();
                continue;
            }
        }
        $this->logRepository->updateField($logId, 'totalItems', $i);
    }
}
