<?php

namespace App\MessageHandler;

use App\Exception\CannotUpdateLogException;
use App\Exception\CannotUpdateMovieException;
use App\Message\DownloadNotification;
use App\Repository\LogRepository;
use App\Repository\MovieRepository;
use App\Util\Download;
use EnvironmentVariableNotExistsException;
use Exception;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

/**
 * Class DownloadNotificationHandler
 *
 * @package App\MessageHandler
 */
class DownloadNotificationHandler implements MessageHandlerInterface
{
    private MovieRepository $movieRepository;
    private Download $download;
    private LogRepository $logRepository;

    /**
     * QueueNotificationHandler constructor.
     *
     * @param MovieRepository $movieRepository
     * @param Download        $download
     * @param LogRepository   $logRepository
     */
    public function __construct(MovieRepository $movieRepository, Download $download, LogRepository $logRepository)
    {
        $this->movieRepository = $movieRepository;
        $this->download = $download;
        $this->logRepository = $logRepository;
    }

    /**
     * @param DownloadNotification $message
     */
    public function __invoke(DownloadNotification $message)
    {
        $data = json_decode($message->getContent(), true);

        $id = $data['id'];
        $identifier = $data['identifier'];

        $items = $data['images'] ?? [];

        $logId = $data['logId'];

        $log = $this->logRepository->findById($logId);

        try {
            $path = env('STORAGE_PATH') . '/';
        } catch (EnvironmentVariableNotExistsException $e) {
            return;
        }

        $images = [];

        foreach ($items as $type => $item) {
            $i = 0;
            foreach ($item as $image) {
                try {
                    $url = explode('.', $image['url']);
                    $ext = end($url);
                    $file = $type . '_' . $identifier . '_' . $i . '.' . $ext;
                    $this->download->download($image['url'], $path . $file);
                    ++$i;
                    $images[$type][] = $file;
                } catch (Exception $e) {
                    echo $e->getMessage();
                    continue;
                }
            }
        }

        try {
            $this->logRepository->incField($logId, 'finishedItems');
            if ($log->getTotalItems() === ($log->getFinishedItems() + 1)) {
                $this->logRepository->updateField($logId, 'status', 'finished');
            }
            $this->movieRepository->updateField($id, 'images', $images);
        } catch (CannotUpdateMovieException $e) {
            return;
        } catch (CannotUpdateLogException $e) {
            return;
        }
    }
}
