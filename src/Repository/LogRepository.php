<?php

namespace App\Repository;

use App\Document\Log;
use App\Exception\CannotCreateLogException;
use App\Exception\CannotUpdateLogException;
use Doctrine\ODM\MongoDB\Repository\DocumentRepository;
use Exception;
use MongoDB\BSON\ObjectId;

/**
 * Class LogRepository
 *
 * @package App\Repository
 */
class LogRepository extends DocumentRepository
{
    /**
     * @param string $id
     *
     * @return Log|null
     */
    public function findById(string $id): ?Log
    {
        try {
            /**
             * @var Log $log
             */
            $log = $this->dm->getRepository(Log::class)->find($id);
            $this->dm->flush();
            return $log;
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * @return Log|null
     */
    public function getLatest(): ?Log
    {
        try {
            /**
             * @var Log $log
             */
            $log = $this->dm->createQueryBuilder(Log::class)
                            ->find()
                            ->refresh()
                            ->sort('_id', -1)
                            ->getQuery()
                            ->getSingleResult();
            $this->dm->flush();
            return $log;
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * @return array
     */
    public function list(): array
    {
        try {
            return $this->dm->getRepository(Log::class)->findAll();
        } catch (Exception $e) {
            return [];
        }
    }

    /**
     * @param Log $log
     *
     * @return Log
     * @throws CannotCreateLogException
     */
    public function create(Log $log): Log
    {
        try {
            $log->setId((new ObjectId())->__toString());
            $this->dm->persist($log);
            $this->dm->flush();
            $this->dm->refresh($log);
            return $log;
        } catch (Exception $e) {
            throw new CannotCreateLogException($e->getMessage());
        }
    }

    /**
     * @param string $id
     * @param string $field
     * @param        $data
     *
     * @return void
     * @throws CannotUpdateLogException
     */
    public function updateField(string $id, string $field, $data)
    {
        try {
            $this->dm->createQueryBuilder(Log::class)
                     ->findAndUpdate()
                     ->refresh(true)
                     ->field('_id')->equals($id)
                     ->field($field)->set($data)
                     ->getQuery()
                     ->execute();
            $this->dm->flush();
        } catch (Exception $e) {
            throw new CannotUpdateLogException($e->getMessage());
        }
    }

    /**
     * @param string $id
     * @param string $field
     *
     * @return void
     * @throws CannotUpdateLogException
     */
    public function incField(string $id, string $field)
    {
        try {
            $this->dm->createQueryBuilder(Log::class)
                     ->findAndUpdate()
                     ->refresh(true)
                     ->field('_id')->equals($id)
                     ->field($field)->inc(1)
                     ->getQuery()
                     ->execute();
            $this->dm->flush();
        } catch (Exception $e) {
            throw new CannotUpdateLogException($e->getMessage());
        }
    }
}
