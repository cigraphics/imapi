<?php

namespace App\Repository;

use App\Document\Movie;
use App\Exception\CannotCreateMovieException;
use App\Exception\CannotUpdateMovieException;
use Doctrine\ODM\MongoDB\MongoDBException;
use Doctrine\ODM\MongoDB\Repository\DocumentRepository;
use Exception;
use MongoDB\BSON\ObjectId;

/**
 * Class MovieRepository
 *
 * @package App\Repository
 */
class MovieRepository extends DocumentRepository
{
    /**
     * @param string $id
     *
     * @return Movie|null
     */
    public function findById(string $id): ?Movie
    {
        try {
            /**
             * @var Movie $movie
             */
            $movie = $this->dm->getRepository(Movie::class)->find($id);
            return $movie;
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * @return array
     */
    public function list(): array
    {
        try {
            return $this->dm->getRepository(Movie::class)->findAll();
        } catch (Exception $e) {
            return [];
        }
    }

    /**
     * @param Movie $movie
     *
     * @return Movie
     * @throws CannotCreateMovieException
     */
    public function create(Movie $movie): Movie
    {
        try {
            $movie->setId((new ObjectId())->__toString());
            $this->dm->persist($movie);
            $this->dm->flush();
            return $movie;
        } catch (Exception $e) {
            throw new CannotCreateMovieException($e->getMessage());
        }
    }

    /**
     * @param string $id
     * @param string $field
     * @param        $data
     *
     * @return void
     * @throws CannotUpdateMovieException
     */
    public function updateField(string $id, string $field, $data)
    {
        try {
            $this->dm->createQueryBuilder(Movie::class)
                ->findAndUpdate()
                ->field('_id')->equals($id)
                ->field($field)->set($data)
                ->getQuery()
                ->execute();
            $this->dm->flush();
        } catch (Exception $e) {
            throw new CannotUpdateMovieException($e->getMessage());
        }
    }

    /**
     * @throws MongoDBException
     */
    public function removeAll()
    {
        $this->dm->getDocumentCollection(Movie::class)->deleteMany([]);
        $this->dm->flush();
    }
}
