<?php

namespace App\Validation;

use App\Exception\ValidationException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ConstraintViolationInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class BaseValidation
 *
 * @package App\Validation
 */
class BaseValidation
{

    /**
     * @var ValidatorInterface
     */
    protected ValidatorInterface $validator;

    /**
     * DictionaryValidation constructor.
     *
     * @param ValidatorInterface $validator
     */
    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @param            $input
     * @param array      $fields
     * @param array      $options
     *
     * @return JsonResponse|null
     * @throws ValidationException
     */
    protected function validate($input, array $fields, array $options = []): ?JsonResponse
    {
        //['allowExtraFields' => true, 'allowMissingFields' => true]

        $constraint = new Assert\Collection(
            array_merge(
                [
                    'fields' => $fields
                ],
                $options
            )
        );

        $violationList = $this->validator->validate($input, $constraint);

        //return count($violationList) ? $this->jsonView('App\View\ErrorView::list', $violationList, 400) : null;
        if (count($violationList)) {
            throw new ValidationException($this->_list($violationList));
        }
        return null;
    }

    /**
     * @param object $collection
     *
     * @return string
     */
    private function _list(object $collection): string
    {
        $errors = [];

        foreach ($collection as $error) {
            $errors[] = $this->_single($error);
        }

        return json_encode(
            [
                'code'    => 400,
                'message' => 'Input validation error.',
                'errors'  => $errors
            ]
        );
    }

    /**
     * @param ConstraintViolationInterface $error
     *
     * @return array
     */
    private function _single(ConstraintViolationInterface $error): array
    {
        return [
            'field'   => $error->getPropertyPath(),
            'message' => str_replace('"', '', $error->getMessage()),
        ];
    }

    /**
     * @return string
     */
    protected function _urlRegEx(): string
    {
        return "/^(http|https):\/\/[\w.-]+[\w\-\._~:\/\?#\[\]@!\$&\%\(\)\*\+,;=.$]+$/i";
    }

}
