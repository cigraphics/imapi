<?php

namespace App\Validation;

use App\Exception\ValidationException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class MainValidation
 *
 * @package App\Validation
 */
class MainValidation extends BaseValidation
{
    /**
     * @param Request $request
     *
     * @return JsonResponse|null
     * @throws ValidationException
     */
    public function queue(Request $request): ?JsonResponse
    {
        $fields = [
            'url'   => [
                new Assert\NotBlank(),
                new Assert\Regex($this->_urlRegEx())
            ],
            'clear' => new Assert\Optional(
                new Assert\Type('bool')
            )
        ];

        return $this->validate($request->request->all(), $fields);
    }
}
