<?php

namespace App\Storage;

use App\Document\Movie;
use App\Repository\MovieRepository;
use Psr\Cache\CacheItemPoolInterface;
use Psr\Cache\InvalidArgumentException;

/**
 * Class MovieStorage
 *
 * @package App\Storage
 */
class MovieStorage
{
    /**
     * @var MovieRepository
     */
    private MovieRepository $repository;

    /**
     * @var CacheItemPoolInterface
     */
    private CacheItemPoolInterface $cache;

    /**
     * UserStorage constructor.
     *
     * @param MovieRepository        $repository
     * @param CacheItemPoolInterface $adapter
     */
    public function __construct(
        MovieRepository $repository,
        CacheItemPoolInterface $adapter
    ) {
        $this->repository = $repository;
        $this->cache = $adapter;
    }

    /**
     * @param string $id
     *
     * @return MovieRepository|null
     * @throws InvalidArgumentException
     */
    public function fetchById(string $id): ?Movie
    {
        $cachedValue = $this->cache->getItem($this->buildCacheKeyById($id));

        if ($cachedValue->isHit()) {
            /**
             * @var Movie $movieData
             */
            $movieData = $cachedValue->get();

            return $movieData;
        }

        return $this->repository->findById($id);
    }

    /**
     * @param string $id
     *
     * @return string
     */
    private function buildCacheKeyById(string $id): string
    {
        return 'storage.movie.' . $id;
    }

    /**
     * @param Movie $movie
     *
     * @throws InvalidArgumentException
     */
    public function clearById(Movie $movie)
    {
        $this->cache->deleteItem($this->buildCacheKeyById($movie->getId()));
    }
}
