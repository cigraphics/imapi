<?php

namespace App\Controller;

use App\Document\Log;
use App\Exception\CannotCreateLogException;
use App\Exception\ValidationException;
use App\Message\QueueNotification;
use App\Repository\LogRepository;
use App\Validation\MainValidation;
use MongoDB\BSON\ObjectId;
use Nelmio\ApiDocBundle\Annotation\Operation;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class MainController
 *
 * @package App\Controller
 * @Route("/api")
 */
class MainController extends BaseController
{
    /**
     * @var MainValidation
     */
    private MainValidation $validate;

    private LogRepository $logRepository;

    /**
     * MainController constructor.
     *
     * @param MainValidation $validate
     * @param LogRepository  $logRepository
     */
    public function __construct(
        MainValidation $validate,
        LogRepository $logRepository
    ) {
        $this->validate = $validate;
        $this->logRepository = $logRepository;
    }

    /**
     * @return Response
     */
    public function docs(): Response
    {
        $file = $this->getParameter('kernel.project_dir') . '/public/doc/apidoc.html';
        return Response::create(file_get_contents($file), 200);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     * @throws ValidationException
     * @Route("/queue", methods={"POST"}, name="app.queue")
     * @Operation(
     *     tags={"API"},
     *     summary="Queue actions",
     *     @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          required=true,
     *          @SWG\Schema(
     *              type="object",
     *              required={"url"},
     *              @SWG\Property(
     *                  property="url",
     *                  description="Url",
     *                  type="string",
     *                  example="http://nginx/data.json",
     *              ),
     *              @SWG\Property(
     *                  property="clear",
     *                  description="Clear current items",
     *                  type="bool",
     *                  default=false,
     *                  example=true,
     *              ),
     *          ),
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="Missing required items"
     *     ),
     *     @SWG\Response(
     *         response="404",
     *         description="Not found"
     *     ),
     * )
     */
    public function queueAction(Request $request): JsonResponse
    {
        $this->validate->queue($request);

        $url = $request->get('url', null);
        $clear = $request->get('clear', false);

        try {
            $log = new Log();
            $log->setId((new ObjectId())->__toString());
            $log->setDate(new \DateTime());
            $log->setStatus('ingesting');
            $this->logRepository->create($log);

            $message = ['url' => $url, 'logId' => $log->getId(), 'clear' => $clear ?? false];

            $this->dispatchMessage(new QueueNotification(json_encode($message)));
            return $this->successResponse('Ingestion has been queued.', 200, ['logId' => $log->getId()]);
        } catch (CannotCreateLogException $e) {
            return $this->errorResponse($e->getMessage());
        } catch (\Exception $e) {
            return $this->errorResponse($e->getMessage());
        }
    }

    /**
     * @return Response
     */
    public function welcomeApi(): Response
    {
        return $this->redirect('/api/doc');
    }
}
