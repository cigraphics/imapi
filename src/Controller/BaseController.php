<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class BaseController
 *
 * @package App\Controller
 */
abstract class BaseController extends AbstractController
{
    /**
     * @param string $message
     * @param int    $status
     * @param array  $headers
     *
     * @return JsonResponse
     */
    protected function notFoundResponse(
        string $message = 'Not Found',
        int $status = 404,
        array $headers = []
    ): JsonResponse {
        return $this->json(
            [
                'code'    => $status,
                'message' => $message,
            ],
            $status,
            $headers
        );
    }

    /**
     * @param       $view
     * @param       $data
     * @param int   $status
     * @param array $headers
     * @param array $context
     *
     * @return JsonResponse
     */
    protected function jsonView($view, $data, $status = 200, $headers = [], $context = [])
    {
        $data = $this->view($view, $data);

        return $this->json($data, $status, $headers, $context);
    }

    /**
     * @param $view
     * @param $data
     *
     * @return mixed
     */
    protected function view($view, $data)
    {
        if (false === strpos($view, ':')) {
            $class = preg_replace(['#\\\\Controller\\\\#', '#Controller$#'], ['\\View\\', 'View'], get_class($this));
        } else {
            $originalView = $view;
            $extractView = explode('::', $originalView);
            $view = end($extractView);
            $class = str_replace('::' . $view, '', $originalView);
        }

        return call_user_func([$this->instantiateClass($class), $view], $data);
    }

    /**
     * @param $class
     *
     * @return mixed
     */
    protected function instantiateClass($class)
    {
        return new $class();
    }

    /**
     * @param string $message
     * @param int    $status
     * @param array  $headers
     *
     * @return JsonResponse
     */
    protected function errorResponse(
        string $message = 'Error',
        int $status = 400,
        array $headers = []
    ): JsonResponse {
        return $this->json(
            [
                'code'    => $status,
                'message' => $message,
            ],
            $status,
            $headers
        );
    }

    /**
     * @param string $message
     * @param int    $status
     * @param array  $headers
     *
     * @return JsonResponse
     */
    protected function unsuccessfulResponse(
        string $message = 'Unsuccessful',
        int $status = 400,
        array $headers = []
    ): JsonResponse {
        return $this->json(
            [
                'code'    => $status,
                'message' => $message,
            ],
            $status,
            $headers
        );
    }

    /**
     * @param string $message
     * @param int    $status
     * @param array  $body
     * @param array  $headers
     *
     * @return JsonResponse
     */
    protected function successResponse(
        string $message = 'Success',
        int $status = 201,
        array $body = [],
        array $headers = []
    ): JsonResponse {
        return $this->json(
            array_merge(
                [
                    'code'    => $status,
                    'message' => $message,
                ],
                $body
            ),
            $status,
            $headers
        );
    }
}
