<?php

namespace App\Controller;

use App\Repository\LogRepository;
use Exception;
use Nelmio\ApiDocBundle\Annotation\Operation;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class LogController
 *
 * @package App\Controller
 * @Route("/api/logs")
 */
class LogController extends BaseController
{
    /**
     * @var LogRepository
     */
    private LogRepository $repository;

    /**
     * UserController constructor.
     *
     * @param LogRepository $repository
     */
    public function __construct(LogRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return JsonResponse
     * @Route("/", methods={"GET"}, name="app.logs.list")
     * @Operation(
     *     tags={"Log"},
     *     summary="List logs",
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     ),
     *     @SWG\Response(
     *         response="404",
     *         description="Not Found"
     *     )
     * )
     */
    public function listAction(): JsonResponse
    {
        $results = $this->repository->list();

        return $this->jsonView('list', $results);
    }

    /**
     * @return JsonResponse
     * @Route("/latest", methods={"GET"}, name="app.logs.latest")
     * @Operation(
     *     tags={"Log"},
     *     summary="Get latest log",
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     ),
     *     @SWG\Response(
     *         response="404",
     *         description="Not Found"
     *     )
     * )
     */
    public function latestAction(): JsonResponse
    {
        $result = $this->repository->getLatest();

        return $this->jsonView('single', $result);
    }

    /**
     * @param string $id
     *
     * @return JsonResponse
     * @Route("/log/{id}", methods={"GET"}, name="app.logs.show", requirements={"id": "\w+"})
     * @Operation(
     *     tags={"Log"},
     *     summary="Show log",
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     ),
     *     @SWG\Response(
     *         response="404",
     *         description="Not Found"
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="Bad request"
     *     )
     * )
     */
    public function showAction(string $id): JsonResponse
    {
        try {
            $object = $this->repository->findById($id);
            if (null === $object) {
                return $this->notFoundResponse();
            }
            return $this->jsonView('single', $object);
        } catch (Exception $e) {
            return $this->unsuccessfulResponse();
        }
    }
}
