<?php

namespace App\Controller;

use App\Repository\MovieRepository;
use App\Storage\MovieStorage;
use EnvironmentVariableNotExistsException;
use Nelmio\ApiDocBundle\Annotation\Operation;
use Psr\Cache\InvalidArgumentException;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class MovieController
 *
 * @package App\Controller
 * @Route("/api/movies")
 */
class MovieController extends BaseController
{
    /**
     * @var MovieRepository
     */
    private MovieRepository $repository;

    /**
     * @var MovieStorage
     */
    private MovieStorage $storage;

    /**
     * UserController constructor.
     *
     * @param MovieRepository $repository
     * @param MovieStorage    $storage
     */
    public function __construct(
        MovieRepository $repository,
        MovieStorage $storage
    ) {
        $this->repository = $repository;
        $this->storage = $storage;
    }

    /**
     * @return JsonResponse
     * @Route("/", methods={"GET"}, name="app.movies.list")
     * @Operation(
     *     tags={"Movie"},
     *     summary="List movies",
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     ),
     *     @SWG\Response(
     *         response="404",
     *         description="Not Found"
     *     )
     * )
     */
    public function listAction(): JsonResponse
    {
        $results = $this->repository->list();

        return $this->jsonView('list', $results);
    }

    /**
     * @param string $id
     *
     * @return JsonResponse
     * @Route("/{id}", methods={"GET"}, name="app.movies.show", requirements={"id": "\w+"})
     * @Operation(
     *     tags={"Movie"},
     *     summary="Show Movie",
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     ),
     *     @SWG\Response(
     *         response="404",
     *         description="Not Found"
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="Bad request"
     *     )
     * )
     */
    public function showAction(string $id): JsonResponse
    {
        try {
            $object = $this->storage->fetchById($id);
            if (null === $object) {
                return $this->notFoundResponse();
            }
            return $this->jsonView('single', $object);
        } catch (InvalidArgumentException $e) {
            return $this->unsuccessfulResponse();
        }
    }

    /**
     * @param string $image
     *
     * @return Response
     * @Route("/proxy/{image}", methods={"GET"}, name="app.movies.proxy")
     * @Operation(
     *     tags={"Movie"},
     *     summary="Get image",
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful"
     *     ),
     *     @SWG\Response(
     *         response="404",
     *         description="Not Found"
     *     ),
     *     @SWG\Response(
     *         response="400",
     *         description="Bad request"
     *     )
     * )
     */
    public function proxyAction(string $image): Response
    {
        try {
            $storage = env('STORAGE_PATH');
        } catch (EnvironmentVariableNotExistsException $e) {
            return $this->errorResponse();
        }

        $file = $storage . '/' . $image;

        if (file_exists($file)) {
            $info = new \SplFileInfo($file);
            switch ($info->getExtension()) {
                case 'png':
                    $mime = 'image/png';
                    break;
                case 'gif':
                    $mime = 'image/gif';
                    break;
                default:
                    $mime = 'image/jpeg';
                    break;
            }
            return new Response(file_get_contents($file), 200, ['Content-Type' => $mime]);
        } else {
            return $this->notFoundResponse();
        }
    }
}
