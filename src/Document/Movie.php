<?php

namespace App\Document;

use DateTime;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @package App\Document
 * @MongoDB\Document(collection="movies", repositoryClass="App\Repository\MovieRepository")
 */
class Movie
{
    /**
     * @MongoDB\Id()
     * @var string|null
     */
    private ?string $id = null;

    /**
     * @MongoDB\Field(type="string")
     * @MongoDB\UniqueIndex(name="_identifier")
     * @var string|null
     */
    private ?string $identifier = null;

    /**
     * @MongoDB\Field(type="string")
     * @var string|null
     */
    private ?string $headline = null;

    /**
     * @MongoDB\Field(type="collection")
     * @var array|null
     */
    private ?array $images = null;

    /**
     * @MongoDB\Field(type="date")
     * @var DateTime|null
     */
    private ?DateTime $lastUpdated = null;

    /**
     * @MongoDB\Field(type="raw")
     * @var array|null
     */
    private ?array $metadata = null;

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param string|null $id
     *
     * @return Movie
     */
    public function setId(?string $id): Movie
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    /**
     * @param string|null $identifier
     *
     * @return Movie
     */
    public function setIdentifier(?string $identifier): Movie
    {
        $this->identifier = $identifier;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getHeadline(): ?string
    {
        return $this->headline;
    }

    /**
     * @param string|null $headline
     *
     * @return Movie
     */
    public function setHeadline(?string $headline): Movie
    {
        $this->headline = $headline;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getImages(): ?array
    {
        return $this->images;
    }

    /**
     * @param array|null $images
     *
     * @return Movie
     */
    public function setImages(?array $images): Movie
    {
        $this->images = $images;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getLastUpdated(): ?DateTime
    {
        return $this->lastUpdated;
    }

    /**
     * @param DateTime|null $lastUpdated
     *
     * @return Movie
     */
    public function setLastUpdated(?DateTime $lastUpdated): Movie
    {
        $this->lastUpdated = $lastUpdated;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getMetadata(): ?array
    {
        return $this->metadata;
    }

    /**
     * @param array|null $metadata
     *
     * @return Movie
     */
    public function setMetadata(?array $metadata): Movie
    {
        $this->metadata = $metadata;
        return $this;
    }
}
