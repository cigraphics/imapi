<?php

namespace App\Document;

use DateTime;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * Class Log
 *
 * @package App\Document
 * @MongoDB\Document(collection="logs", repositoryClass="App\Repository\LogRepository")
 */
class Log
{
    /**
     * @MongoDB\Id()
     * @var string
     */
    private string $id = "";

    /**
     * @MongoDB\Field(type="string")
     * @var string
     */
    private string $status = "";

    /**
     * @MongoDB\Field(type="int")
     * @var int
     */
    private int $totalItems = 0;

    /**
     * @MongoDB\Field(type="int")
     * @var int
     */
    private int $finishedItems = 0;

    /**
     * @MongoDB\Field(type="date")
     * @var DateTime
     */
    private DateTime $date;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     *
     * @return Log
     */
    public function setId(string $id): Log
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     *
     * @return Log
     */
    public function setStatus(string $status): Log
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return int
     */
    public function getTotalItems(): int
    {
        return $this->totalItems;
    }

    /**
     * @param int $totalItems
     *
     * @return Log
     */
    public function setTotalItems(int $totalItems): Log
    {
        $this->totalItems = $totalItems;
        return $this;
    }

    /**
     * @return int
     */
    public function getFinishedItems(): int
    {
        return $this->finishedItems;
    }

    /**
     * @param int $finishedItems
     *
     * @return Log
     */
    public function setFinishedItems(int $finishedItems): Log
    {
        $this->finishedItems = $finishedItems;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDate(): DateTime
    {
        return $this->date;
    }

    /**
     * @param DateTime $date
     *
     * @return Log
     */
    public function setDate(DateTime $date): Log
    {
        $this->date = $date;
        return $this;
    }
}
