<?php

namespace App\Message;

/**
 * Class DownloadNotification
 *
 * @package App\Message
 */
class DownloadNotification
{
    /**
     * @var string
     */
    private string $content = "";

    /**
     * QueueNotification constructor.
     *
     * @param string $content
     */
    public function __construct(string $content)
    {
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }
}
