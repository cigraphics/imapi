<?php

namespace App\Message;

/**
 * Class QueueNotification
 *
 * @package App\Message
 */
class QueueNotification
{
    /**
     * @var string
     */
    private string $content = "";

    /**
     * QueueNotification constructor.
     *
     * @param string $content
     */
    public function __construct(string $content)
    {
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }
}
