<?php

namespace App\Util;

use Exception;

/**
 * Class Download
 *
 * @package App\Util
 */
class Download
{
    use LoggerTrait;

    /**
     * @param string $url
     *
     * @return bool|string
     * @throws Exception
     */
    public function getUrl(string $url)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FAILONERROR, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt(
            $ch,
            CURLOPT_USERAGENT,
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36"
        );
        $data = curl_exec($ch);

        if (curl_errno($ch)) {
            $error = curl_error($ch);
            throw new Exception($error);
        }

        curl_close($ch);

        return $data;
    }

    /**
     * @param string $url
     *
     * @return array
     */
    public function getJson(string $url): array
    {
        try {
            $data = $this->getUrl($url);
            return json_decode(utf8_decode($data), true);
        } catch (Exception $e) {
            return [];
        }
    }

    /**
     * @param string $url
     * @param string $output
     *
     * @throws Exception
     */
    public function download(string $url, string $output)
    {
        $data = $this->getUrl($url);
        file_put_contents($output, $data);
    }
}
