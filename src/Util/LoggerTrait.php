<?php

namespace App\Util;

use Monolog\Logger;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Throwable;

/**
 * Trait LoggerTrait
 *
 * @package App\Util
 */
trait LoggerTrait
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param LoggerInterface $logger
     *
     * @return $this
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;

        return $this;
    }

    /**
     * @param string $message
     * @param null   $args
     *
     * @return bool
     */
    public function logDebug(string $message, $args = null): bool
    {
        return $this->log(Logger::DEBUG, sprintf(...func_get_args()));
    }

    /**
     * @param int    $level
     * @param string $message
     * @param array  $context
     *
     * @return bool
     */
    public function log(int $level, string $message, array $context = []): bool
    {
        if (null !== $this->logger) {
            $this->logger->log($level, $message, $context);

            return true;
        }

        return false;
    }

    /**
     * @param string $message
     * @param null   $args
     *
     * @return bool
     */
    public function logInfo(string $message, $args = null): bool
    {
        return $this->log(Logger::INFO, sprintf(...func_get_args()));
    }

    /**
     * @param string $message
     * @param null   $args
     *
     * @return bool
     */
    public function logNotice(string $message, $args = null): bool
    {
        return $this->log(Logger::NOTICE, sprintf(...func_get_args()));
    }

    /**
     * @param string $message
     * @param null   $args
     *
     * @return bool
     */
    public function logWarn(string $message, $args = null): bool
    {
        return $this->log(Logger::WARNING, sprintf(...func_get_args()));
    }

    /**
     * @param string $message
     * @param null   $args
     *
     * @return bool
     */
    public function logError(string $message, $args = null): bool
    {
        return $this->log(Logger::ERROR, sprintf(...func_get_args()));
    }

    /**
     * @param string $message
     * @param null   $args
     *
     * @return bool
     */
    public function logCritical(string $message, $args = null): bool
    {
        return $this->log(Logger::CRITICAL, sprintf(...func_get_args()));
    }

    /**
     * @param string $message
     * @param null   $args
     *
     * @return bool
     */
    public function logAlert(string $message, $args = null): bool
    {
        return $this->log(Logger::ALERT, sprintf(...func_get_args()));
    }

    /**
     * @param string $message
     * @param null   $args
     *
     * @return bool
     */
    public function logEmergency(string $message, $args = null): bool
    {
        return $this->log(Logger::EMERGENCY, sprintf(...func_get_args()));
    }

    /**
     * Logs an exception.
     *
     * @param Throwable $exception The \Exception instance
     * @param string    $message   The error message to log
     */
    protected function logException(Throwable $exception, $message)
    {
        if (null !== $this->logger) {
            if (!$exception instanceof HttpExceptionInterface || $exception->getStatusCode() >= 500) {
                $this->logger->critical($message, ['exception' => $exception]);
            } else {
                $this->logger->error($message, ['exception' => $exception]);
            }
        }
    }
}
