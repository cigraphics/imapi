<?php

namespace App\EventListener;

use App\Exception\ValidationException;
use App\Util\LoggerTrait;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ExceptionListener
 *
 * @package App\EventListener
 */
class ExceptionListener
{
    use LoggerTrait;

    /**
     * @param ExceptionEvent $event
     */
    public function onKernelException(ExceptionEvent $event)
    {
        $exception = $event->getThrowable();

        $this->logException(
            $exception,
            sprintf(
                'Uncaught PHP Exception %s: "%s" at %s line %s',
                get_class($exception),
                $exception->getMessage(),
                $exception->getFile(),
                $exception->getLine()
            )
        );

        $httpStatusCode = 500;

        if ($exception instanceof NotFoundHttpException) {
            $response = [
                'code'    => 404,
                'message' => $exception->getMessage(),
            ];
            $httpStatusCode = 404;
        } elseif ($exception instanceof AccessDeniedHttpException) {
            $response = [
                'code'    => 403,
                'message' => $exception->getMessage(),
            ];
            $httpStatusCode = 403;
        } elseif ($exception instanceof ValidationException) {
            $message = json_decode($exception->getMessage(), true);
            $response = [
                'code'    => $message['code'],
                'message' => $message['message'],
                'errors'  => $message['errors'],
            ];
            $httpStatusCode = 400;
        } else {
            $response = [
                'code'    => 500,
                'message' => $exception->getMessage(),
            ];
        }

        if ($event->getRequest()->query->has('_full')) {
            $response['class'] = get_class($exception);
            $response['stacktrace'] = $exception->getTrace();
        }

        $event->setResponse(new JsonResponse($response, $httpStatusCode));
    }
}
