<?php

namespace App\EventListener;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;

/**
 * Class JsonEventListener
 *
 */
class JsonEventListener
{
    /**
     * @param RequestEvent $event
     */
    public function onKernelRequest(RequestEvent $event)
    {
        $request = $event->getRequest();

        if (false === strpos($request->getContentType(), 'json')) {
            return;
        }

        if ($request->attributes->has('_ignore_json_body')) {
            return;
        }

        $content = $request->getContent(false);

        if (empty($content)) {
            return;
        }

        if (false === ($params = $this->parseJson($content))) {
            $event->setResponse(new JsonResponse(['error' => 'Unable to parse JSON'], 400));
            return;
        }

        $request->request->replace($params);
    }

    /**
     * @param $content
     *
     * @return bool|mixed
     */
    private function parseJson($content)
    {
        $data = json_decode($content, true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            return false;
        }

        return $data;
    }
}
