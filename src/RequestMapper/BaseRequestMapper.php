<?php

namespace App\RequestMapper;

use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class BaseRequestMapper
 *
 * @package App\RequestMapper
 */
abstract class BaseRequestMapper
{
    /**
     * @param Request $request
     * @param         $object
     *
     * @return mixed
     */
    /**
     * @param Request $request
     * @param         $object
     *
     * @return mixed
     */
    public function mapRequest(Request $request, $object)
    {
        return $this->map($request->request, $object);
    }

    /**
     * @param ParameterBag $bag
     * @param              $object
     *
     * @return mixed
     */
    /**
     * @param ParameterBag $bag
     * @param              $object
     *
     * @return mixed
     */
    abstract public function map(ParameterBag $bag, $object);

    /**
     * @param ParameterBag $bag
     * @param              $object
     *
     * @return mixed
     */
    /**
     * @param ParameterBag $bag
     * @param              $object
     *
     * @return mixed
     */
    public function mapBag(ParameterBag $bag, $object)
    {
        return $this->map($bag, $object);
    }

    /**
     * @param              $object
     * @param string       $method
     * @param string       $key
     * @param ParameterBag $bag
     *
     * @return $this
     */
    /**
     * @param              $object
     * @param string       $method
     * @param string       $key
     * @param ParameterBag $bag
     *
     * @return $this
     */
    protected function assignIfIsset($object, string $method, string $key, ParameterBag $bag)
    {
        if ($bag->has($key)) {
            $object->$method($bag->get($key));
        }

        return $this;
    }
}
