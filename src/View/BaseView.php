<?php

namespace App\View;

//use App\Util\Thruster\ViewBundle\View\View;
use DateTime;

/**
 * Class BaseView
 *
 * @package App\View
 */
class BaseView
{
    /**
     * @param string|array $view A short notation view (a:b:c) AppBundle:Default:homepage
     * @param array        $data
     * @param bool         $preserveKeys
     *
     * @return array
     */
    public function renderMany($view, $data, $preserveKeys = false)
    {
        $result = [];

        foreach ($data as $key => $item) {
            if (true === $preserveKeys) {
                $result[$key] = $this->renderOne($view, $item);
            } else {
                $result[] = $this->renderOne($view, $item);
            }
        }

        return $result;
    }

    /**
     * @param string|array $view A short notation view (a:b:c) AppBundle:Default:homepage
     * @param mixed        $data
     *
     * @return mixed
     */
    public function renderOne($view, $data)
    {
        if (is_array($view)) {
            return call_user_func($view, $data);
        }
        return $this->view($view, $data);
    }

    /**
     * @param $view
     * @param $data
     *
     * @return mixed
     */
    protected function view($view, $data)
    {
        if (false === strpos($view, ':')) {
            $class = preg_replace(['#\\\\Controller\\\\#', '#Controller$#'], ['\\View\\', 'View'], get_class($this));
        } else {
            $originalView = $view;
            $extractView = explode('::', $originalView);
            $view = end($extractView);
            $class = str_replace('::' . $view, '', $originalView);
        }

        return call_user_func([$this->instantiateClass($class), $view], $data);
    }

    /**
     * @param $class
     *
     * @return mixed
     */
    protected function instantiateClass($class)
    {
        return new $class();
    }

    /**
     * @param DateTime|null $dateTime
     * @param null          $default
     *
     * @return int|null
     */
    protected function toTimestamp(?DateTime $dateTime, $default = null)
    {
        if (null === $dateTime) {
            return $default;
        }

        return $dateTime->getTimestamp();
    }
}
