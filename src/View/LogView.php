<?php

namespace App\View;

use App\Document\Log;

/**
 * Class Log
 *
 * @package App\View
 */
class LogView extends BaseView
{
    /**
     * @param array $collection
     *
     * @return array
     */
    public function list(array $collection): array
    {
        return [
            'logs' => $this->renderMany([$this, 'single'], $collection)
        ];
    }

    /**
     * @param Log $log
     *
     * @return array
     */
    public function single(Log $log): array
    {
        return [
            'id'            => $log->getId(),
            'status'        => $log->getStatus(),
            'totalItems'    => $log->getTotalItems(),
            'finishedItems' => $log->getFinishedItems(),
            'date'          => $log->getDate(),
        ];
    }
}
