<?php

namespace App\View;

use Symfony\Component\Validator\ConstraintViolationInterface;

/**
 * Class ErrorView
 *
 * @package App\View
 */
class ErrorView extends BaseView
{
    /**
     * @param array|object $collection
     *
     * @return array
     */
    public function list(object $collection): array
    {
        return [
            'code'    => 400,
            'message' => 'Input validation error.',
            'errors'  => $this->renderMany([$this, 'single'], $collection)
        ];
    }

    /**
     * @param ConstraintViolationInterface $error
     *
     * @return array
     */
    public function single(ConstraintViolationInterface $error): array
    {
        return [
            'field'   => $error->getPropertyPath(),
            'message' => str_replace('"', '', $error->getMessage()),
        ];
    }
}
