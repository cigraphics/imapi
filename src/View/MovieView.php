<?php

namespace App\View;

use App\Document\Movie;

/**
 * Class Movie
 *
 * @package App\View
 */
class MovieView extends BaseView
{
    /**
     * @param array $collection
     *
     * @return array
     */
    public function list(array $collection): array
    {
        return [
            'movies' => $this->renderMany([$this, 'singleList'], $collection)
        ];
    }

    /**
     * @param Movie $movie
     *
     * @return array
     */
    public function single(Movie $movie): array
    {
        return [
            'id'          => $movie->getId(),
            'identifier'  => $movie->getIdentifier(),
            'headline'    => $movie->getHeadline(),
            'images'      => $movie->getImages(),
            'metadata'    => $movie->getMetadata(),
            'lastUpdated' => $movie->getLastUpdated(),
        ];
    }

    /**
     * @param Movie $movie
     *
     * @return array
     */
    public function singleList(Movie $movie): array
    {
        return [
            'id'          => $movie->getId(),
            'identifier'  => $movie->getIdentifier(),
            'headline'    => $movie->getHeadline(),
            'images'      => $movie->getImages(),
        ];
    }
}
