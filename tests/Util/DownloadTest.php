<?php
namespace App\Tests\Util;

use App\Util\Download;
use PHPUnit\Framework\TestCase;

/**
 * Class DownloadTest
 *
 * @package App\Tests\Util
 */
final class DownloadTest extends TestCase
{
    private const VALID_URL = 'http://nginx/data.json';
    private const INVALID_URL = 'http://invalid.url/query';

    private Download $download;

    /**
     * @param string $name
     * @param string $dataName
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->download = new Download();
    }

    public function testValidUrl(): void
    {
        static::assertIsString($this->download->getUrl(self::VALID_URL));
    }

    public function testInvalidUrl(): void
    {
        static::expectExceptionMessage("Could not resolve host: invalid.url");
        $this->download->getUrl(self::INVALID_URL);
    }

    public function testJsonOutput(): void
    {
        static::assertIsArray($this->download->getJson(self::VALID_URL));
    }
}
