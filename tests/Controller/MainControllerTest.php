<?php
namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class MainControllerTest
 *
 * @package App\Tests\Controller
 */
class MainControllerTest extends WebTestCase
{
    private const INGEST_URL = 'http://nginx/data.json';

    public function testQueuePost()
    {
        $client = static::createClient();
        $client->request('POST', 'http://nginx/api/queue', [
            'url' => self::INGEST_URL,
            'clear' => true
        ]);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }
}
