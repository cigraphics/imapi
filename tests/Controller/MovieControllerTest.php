<?php
namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class MovieControllerTest
 *
 * @package App\Tests\Controller
 */
class MovieControllerTest extends WebTestCase
{
    private const INGEST_URL = 'http://nginx/data.json';
    private const INVALID_ID = 'invalid.id.test';

    public function testGetInvalidMovieId()
    {
        $client = static::createClient();
        $client->request('GET', 'http://nginx/api/movies/' . self::INVALID_ID);
        $this->assertEquals(404, $client->getResponse()->getStatusCode());
    }
}
