import axios from 'axios';

export default () => {
    console.log(process.env.REACT_APP_API_URL);
    return (axios.defaults.baseURL = process.env.REACT_APP_API_URL);
};
