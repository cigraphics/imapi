import React, {Fragment} from "react";

function MovieCardImages(movieCardImages, currentImage, changeImage) {
    return <Fragment>
        <img alt={"cardImage"}
             src={process.env.REACT_APP_API_URL + "/movies/proxy/" + (null === currentImage ? movieCardImages[0] : currentImage)}/>
        <div className={'clear'}/>
        {movieCardImages.map((item, index) => {
            return <div key={index} onClick={() => changeImage(item)} className={'thumb'}
                        style={{backgroundImage: 'url(' + process.env.REACT_APP_API_URL + '/movies/proxy/' + item + ')'}}/>
        })}
    </Fragment>
}

export default MovieCardImages;
