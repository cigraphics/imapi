import React from "react";

function MovieList(movie, index, loadMovie) {
    let image = movie?.images?.cardImages[0] ? process.env.REACT_APP_API_URL + "/movies/proxy/" + movie.images.cardImages[0] : '/noimage.jpg';
    return (<div className={'card'} key={index} onClick={() => loadMovie(movie.id)}
                 style={{backgroundImage: "url(" + image + ")"}}>
        <span>{movie.headline}</span>
    </div>)
}

export default MovieList;
