import React, {Fragment} from "react";
import MovieCardImages from "./movieCardImages";

function Movie(movie, currentImage, changeImage, resetAll) {
    let metadata = movie.metadata;

    const parseMetadata = metadata => {
        let keys = [];
        for(let key in metadata) {
            keys.push({name: key, value: metadata[key]});
        }
        return keys;
    };

    const unCamelCase = str => {
        return str.replace(/([A-Z])/g, ' $1')
            // uppercase the first character
            .replace(/^./, function(str){ return str.toUpperCase(); });
    }

    return <Fragment>
        <div id={"movie"}>
            <button onClick={() => resetAll()}>Go back</button>
            <h1>{movie.headline}</h1>
            <div id={"images"}>
                {MovieCardImages(movie?.images?.cardImages, currentImage, changeImage)}
            </div>
            <div id={"movieInfo"}>
                {parseMetadata(metadata).map((item, index) => {
                    if(Array.isArray(item.value)) {
                        return (<div key={index}><span className={'name'}>{unCamelCase(item.name)}:</span> <span
                            className={'value'}>{item.value.map((value, key) => {
                            return (<span className={'innerValue'} key={key}>{value?.name}</span>)
                        })}</span></div>)
                    }else if ("string" === typeof item.value) {
                        return (<div key={index}><span className={'name'}>{unCamelCase(item.name)}:</span> <span className={'value'}>{item.value}</span></div>)
                    } else {
                        return ''
                    }
                })}
            </div>
        </div>
    </Fragment>
}

export default Movie;
