import MovieList from "./movieList";

function MoviesList(list, loadMovie) {
    return list.map((item, index) => {
        return MovieList(item, index, loadMovie);
    })
}

export default MoviesList;
