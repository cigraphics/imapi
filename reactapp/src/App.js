import React, {Fragment, useLayoutEffect, useState} from 'react';
import axios from "axios";
import './App.css';
import MoviesList from "./Movie/moviesList";
import Movie from "./Movie/movie";

function App() {
    const [isIngesting, setIsIngesting] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const [list, setList] = useState([]);
    const [progress, setProgress] = useState(null);
    const [movie, setMovie] = useState({});
    const [hasMovie, setHasMovie] = useState(false);
    const [isMovieLoading, setIsMovieLoading] = useState(false);
    const [currentImage, setCurrentImage] = useState(null);

    let logId;

    useLayoutEffect(() => {
        getMovies();
    }, []);

    const ingest = () => {
        setIsIngesting(true);
        axios.post("/queue", {
            url: process.env.REACT_APP_JSON_DATA,
            clear: true
        }).then(response => {
            logId = response.data.logId;
            checkIngestion();
        }).catch(error => {
            setIsIngesting(false);
            console.log(error);
        });
    };

    const checkIngestion = () => {
        axios.get("/logs/log/" + logId)
            .then(response => {
                setProgress(response.data.finishedItems + '/' + response.data.totalItems);
                if (response.data.status === "ingesting") {
                    // setTimeout(() => {
                    checkIngestion();
                    // }, 100);
                } else {
                    setProgress(null);
                    setIsIngesting(false);
                    getMovies();
                }
            }).catch(error => {
            console.log(error);
        });
    };

    const getMovies = () => {
        setIsLoading(true);
        setHasMovie(false);
        axios.get("/movies/")
            .then(response => {
                setList(response.data.movies);
                setIsLoading(false);
            }).catch(error => {
            setIsLoading(false);
            console.log(error);
        });
    };

    const loadMovie = id => {
        setHasMovie(true);
        setIsMovieLoading(true);
        axios.get("/movies/" + id)
            .then(response => {
                setMovie(response.data);
                setIsMovieLoading(false);
            }).catch(error => {
            setIsMovieLoading(false);
            console.log(error);
        })
    };

    const changeImage = image => {
        console.log(image);
        setCurrentImage(image);
    };

    const resetAll = () => {
        setMovie({});
        setHasMovie(false);
        setCurrentImage(null);
    };

    return (
        <div>
            <header>
                <h1>IMDB</h1>
                {isIngesting ?
                    <button>Ingesting {progress ? progress : '...'}</button>
                    :
                    <button onClick={() => ingest()}>Ingest JSON</button>
                }
            </header>
            <div id={"content"}>
                {hasMovie ?
                    <Fragment>
                        {isMovieLoading ?
                            <div>Loading ...</div>
                            :
                            <div id={"movie"}>{Movie(movie, currentImage, changeImage, resetAll)}</div>
                        }
                    </Fragment>
                    :
                    isLoading ?
                        <div>Loading ...</div>
                        :
                        <Fragment>
                            <div id={"movieList"}>{MoviesList(list, loadMovie)}</div>
                        </Fragment>
                }
            </div>
        </div>
    );
}

export default App;
